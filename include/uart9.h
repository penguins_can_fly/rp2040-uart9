#ifndef RP2040_UART9_UART9_H
#define RP2040_UART9_UART9_H

#include "hardware/uart.h"

namespace uart9 {

struct ReadResult {
    const uint8_t byte;
    const bool ninth_bit;
};

class Inst {
public:
    Inst(uart_inst_t *uart, uint baudrate, uint8_t data_bits, uint8_t stop_bits);

    ReadResult get_c();

    void put_c_raw(uint8_t c, bool ninth_bit);

    [[nodiscard]] bool is_readable() const {
        return uart_is_readable(uart);
    }

    [[nodiscard]] bool is_writable() const {
        return uart_is_readable(uart);
    }

    void tx_wait_blocking() {
        uart_tx_wait_blocking(uart);
    }

    void set_format(uint new_data_bits, uint new_stop_bits) {
        data_bits = new_data_bits;
        stop_bits = new_stop_bits;
        uart_set_format(uart, data_bits, stop_bits, UART_PARITY_EVEN);
    }

    void set_fifo_enabled(const bool enabled) {
        uart_set_fifo_enabled(uart, enabled);
    }

    void set_translate_crlf(const bool translate) {
        uart_set_translate_crlf(uart, translate);
    }

    void set_hw_flow(const bool cts, const bool rts) {
        uart_set_hw_flow(uart, cts, rts);
    }

    void set_irq_enables(const bool rx_has_data, const bool tx_needs_data) {
        uart_set_irq_enables(uart, rx_has_data, tx_needs_data);
    }

private:
    uart_inst_t *uart;
    uart_hw_t *uart_hw;
    uint8_t data_bits;
    uint8_t stop_bits;
};

}  // namespace uart9

#endif  // RP2040_UART9_UART9_H
