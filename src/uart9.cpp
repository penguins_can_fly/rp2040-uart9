#include "uart9.h"

#include <bit>

namespace uart9 {

Inst::Inst(uart_inst_t *uart, uint baudrate, uint8_t data_bits, uint8_t stop_bits)
    : uart(uart), uart_hw(uart_get_hw(uart)), data_bits(data_bits), stop_bits(stop_bits) {
    uart_init(uart, baudrate);
    uart_set_format(uart, data_bits, stop_bits, UART_PARITY_EVEN);  // parity bit will serve as 9th bit
}

ReadResult Inst::get_c() {
    const uint8_t byte = uart_getc(uart);

    const bool parity_error = uart_hw->rsr & UART_UARTRSR_PE_BITS;
    const bool even = std::popcount(byte) % 2;
    hw_clear_bits(&uart_hw->rsr, UART_UARTRSR_BITS);  // clear error flag

    const bool ninth_bit = parity_error && !even;

    return ReadResult{byte, ninth_bit};
}

void Inst::put_c_raw(uint8_t c, bool ninth_bit) {
    const bool is_even = std::popcount(c) % 2 == 0;

    uart_set_format(uart, data_bits, stop_bits, ninth_bit == is_even ? UART_PARITY_ODD : UART_PARITY_EVEN);
    uart_putc_raw(uart, c);

    uart_set_format(uart, data_bits, stop_bits, UART_PARITY_EVEN);  // always reset parity to even
}

}  // namespace uart9
